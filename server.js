var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var redis=require('redis'), subscriber = redis.createClient(), publisher = redis.createClient();

subscriber.on("message", function(channel, msg){
    console.log("Message '" + msg + "' on channel '" + channel + "' arrived!");
    io.sockets.emit('new message', {msg});
})

subscriber.subscribe("chat");

connections = [];

server.listen(3000);
console.log('chat server started...');

app.get('/', function(req,res){
    res.sendFile(__dirname+'/index.html');
})

io.sockets.on('connection', function(socket){
    connections.push(socket);
    console.log('Connected : %s sockets connected', connections.length);

    //Disconnect
    socket.on('disconnect', function(data){
         connections.splice(connections.indexOf(socket), 1);         
        console.log('Disconnected: %s sockets connected', connections.length);
    });

    //send message
    socket.on('send message', function(data){
        console.log('message from ' + connections.indexOf(socket) + ' with content ' + data);
        publisher.publish("chat", data);
    });

})